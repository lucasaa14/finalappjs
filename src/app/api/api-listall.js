import { starWarsLoader } from "../api/functions";


// -----------------------------------------------------CHARACTERS--------------------------------------------------------
// https://swapi.dev/api/people/?page=${pagePlanets}
const fetchStarWarsCharacters = async() => {
    starWarsLoader();
    const url = `https://swapi.dev/api/people/`;
    // pageCharacters += 1;
    const promise = await fetch(url);
    const { results: object } = await promise.json();

    // console.log(object);
    const getIdFromUrl = (url) => url.split('/').slice(-2).join('');
    const characterInfo = object.map(({ name, url }) => ({ name, id: getIdFromUrl(url) }))

    starWarsLoader();

    // console.log(characterInfo);
    // console.log(response.info);
    return characterInfo;


}

// --------------------------------------------------------------PLANETS-------------------------------------------------------
// https://swapi.dev/api/planets/?page=${pagePlanets}
const fetchStarWarsPlanets = async() => {
    starWarsLoader();
    const url = `https://swapi.dev/api/planets/`;
    // pagePlanets += 1;
    const promise = await fetch(url);
    const { results: object } = await promise.json();
    console.log(object);
    const getIdFromUrl = (url) => url.split('/').slice(-2).join('');
    const planetsInfo = object.map(({ name, url }) => ({ name, id: getIdFromUrl(url) }))

    starWarsLoader();
    // console.log(planetsInfo);
    // console.log(response.results);
    return planetsInfo;


}

//----------------------------------------------------------------STARSHIPS--------------------------------------------------------
// `https://swapi.dev/api/starships/?page=${ pageStarships}
const fetchStarWarsStarships = async() => {
        starWarsLoader();
        const url = `https://swapi.dev/api/starships`;
        // pageStarships += 1;
        const promise = await fetch(url);
        const { results: object } = await promise.json();
        console.log(object);
        const getIdFromUrl = (url) => url.split('/').slice(-2).join('');
        const starShipsInfo = object.map(({ name, url }) => ({ name, id: getIdFromUrl(url) }));

        starWarsLoader();
        // console.log(starShipsInfo);
        // console.log(response.results);
        return starShipsInfo;


    }
    // -----------------------------CHANGES PAGES CHARACTERS--------------------------------
const fetchStarWarsChrtPage = async(url) => {
        starWarsLoader();
        // pageCharacters += 1;
        const promise = await fetch(url);
        const { results: object } = await promise.json();
        console.log(object);
        const getIdFromUrl = (url) => url.split('/').slice(-2).join('');
        const characterInfo = object.map(({ name, url }) => ({ name, id: getIdFromUrl(url) }))

        starWarsLoader();
        // console.log(characterInfo);
        // console.log(response.info);
        return characterInfo;

    }
    // -----------------------------CHANGES PAGES PLANETS--------------------------------
const fetchStarWarsPlntsPage = async(url) => {
        starWarsLoader();
        // pageCharacters += 1;
        const promise = await fetch(url);
        const { results: object } = await promise.json();
        // console.log(object);
        const getIdFromUrl = (url) => url.split('/').slice(-2).join('');
        const planetsInfo = object.map(({ name, url }) => ({ name, id: getIdFromUrl(url) }))

        starWarsLoader();
        // console.log(planetsInfo);
        // console.log(response.info);
        return planetsInfo;

    }
    // -----------------------------CHANGES PAGES STARSHIPS--------------------------------
const fetchStarWarsStshpPage = async(url) => {
    starWarsLoader();
    // pageCharacters += 1;
    const promise = await fetch(url);
    const { results: object } = await promise.json();
    console.log(object);
    const getIdFromUrl = (url) => url.split('/').slice(-2).join('');
    const starShipsInfo = object.map(({ name, url }) => ({ name, id: getIdFromUrl(url) }))

    starWarsLoader();
    // console.log(starShipsInfo);
    // console.log(response.info);
    return starShipsInfo;

}


export { fetchStarWarsCharacters };
export { fetchStarWarsPlanets };
export { fetchStarWarsStarships };
export { fetchStarWarsChrtPage };
export { fetchStarWarsPlntsPage };
export { fetchStarWarsStshpPage };