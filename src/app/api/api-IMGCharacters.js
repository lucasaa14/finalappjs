const findImgCharacters = (starWarsNameId) => {
    let urlImg;
    const imgCharacters = [{
            id: 1,
            name: "Luke Skywalker",
            url: "https://media.contentapi.ea.com/content/dam/star-wars-battlefront-2/images/2019/08/swbf2-refresh-hero-large-heroes-page-luke-skywalker-16x9-xl.jpg.adapt.crop1x1.320w.jpg",

        },
        {
            id: 2,
            name: "C-3PO",
            url: "https://static.wikia.nocookie.net/esstarwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png",

        },
        {
            id: 3,
            name: "R2-D2",
            url: "https://static.wikia.nocookie.net/esstarwars/images/e/eb/ArtooTFA2-Fathead.png",

        },
        {
            id: 4,
            name: "Darth Vader",
            url: "https://hipertextual.com/wp-content/uploads/2020/01/hipertextual-star-wars-deseo-mas-grande-darth-vader-podria-hacerse-realidad-muy-pronto-2020659163.jpg",

        },
        {
            id: 5,
            name: "Leia Organa",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/9b/Princessleiaheadwithgun.jpg",

        },
        {
            id: 6,
            name: "Owen Lars",
            url: "https://static.wikia.nocookie.net/doblaje/images/8/84/Owen_Lars_Young_STAR_WARS.png",

        },
        {
            id: 7,
            name: "Beru Whitesun lars",
            url: "https://static.wikia.nocookie.net/starwars/images/8/84/BeruWhitesunLars.jpg",

        },
        {
            id: 8,
            name: "R5-D4",
            url: "https://static.wikia.nocookie.net/esstarwars/images/c/cb/R5-D4_Sideshow.png",

        },
        {
            id: 9,
            name: "Biggs Darklighter",
            url: "https://static.wikia.nocookie.net/esstarwars/images/4/43/Biggs.png",

        },
        {
            id: 10,
            name: "Obi-Wan Kenobi",
            url: "https://www.cinemascomics.com/wp-content/uploads/2020/10/OBI-WAN-KENOBI.jpg",

        },
        {
            id: 11,
            name: "Anakin Skywalker",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/92/AnakinSkywalker-SWTLC.png"
        },
        {
            id: 12,
            name: "Wilhuff Tarkin",
            url: "https://fichas.universomarvel.com/personajeslicencias/starwarsact/fotos/tarkin.jpg",

        },
        {
            id: 13,
            name: "Chewbacca",
            url: "https://static.wikia.nocookie.net/esstarwars/images/5/51/Chewbacca_TLJ.PNG",

        },
        {
            id: 14,
            name: "Han Solo",
            url: "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/han-solo-1527175935.jpg",

        },
        {
            id: 15,
            name: "Greedo",
            url: "https://static.wikia.nocookie.net/esstarwars/images/c/c6/Greedo.jpg",

        },
        {
            id: 16,
            name: "Jabba Desilijic Tiure",
            url: "https://static.wikia.nocookie.net/esstarwars/images/4/4e/Jabba_HS.jpg",

        },
        {
            id: 18,
            name: "Wedge Antilles",
            url: "https://static.wikia.nocookie.net/esstarwars/images/5/51/Wedge_Antilles.jpg",

        },
        {
            id: 19,
            name: "Jek Tono Porkins",
            url: "https://pbs.twimg.com/profile_images/614431171460624386/Z0pxKl9v.png",

        },
        {
            id: 20,
            name: "Yoda",
            url: "https://static.wikia.nocookie.net/disney/images/9/95/Master_Yoda.png",

        },
        {
            id: 21,
            name: "Palpatine",
            url: "https://wipy.tv/wp-content/uploads/2020/07/edad-de-palpatine-en-star-wars-1200x720.jpg",
        },
        {
            id: 22,
            name: "Boba Fett ",
            url: "https://static.wikia.nocookie.net/esstarwars/images/5/58/BobaFettMain2.jpg ",
        },
        {
            id: 23,
            name: "IG-88",
            url: "https://static.wikia.nocookie.net/esstarwars/images/8/82/IG-88.jpg",
        },
        {
            id: 24,
            name: "Bossk",
            url: "https://static.wikia.nocookie.net/esstarwars/images/1/1d/Bossk.png",
        },
        {
            id: 25,
            name: " Lando Calrissian",
            url: "https://static.wikia.nocookie.net/esstarwars/images/7/7d/Lando_WoSW.jpg",
        },
        {
            id: 26,
            name: "Lobot ",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/96/SWE_Lobot.jpg ",
        },
        {
            id: 27,
            name: "Ackbar ",
            url: "https://static.wikia.nocookie.net/esstarwars/images/2/29/Admiral_Ackbar_RH.png",
        },
        {
            id: 28,
            name: "Mon Mothma ",
            url: " http://pm1.narvii.com/6370/e247b63697b44238bd18e67f8e29af6274dc9791_00.jpg",
        },
        {
            id: 29,
            name: "Arvel Crynyd ",
            url: "https://static.wikia.nocookie.net/esstarwars/images/d/de/Arvel-crynyd.jpg",
        },
        {
            id: 30,
            name: " Wicket Systri Warrick",
            url: " https://static.wikia.nocookie.net/starwars/images/4/41/Wicket-TROS.png",
        },
        {
            id: 31,
            name: "Nien Nunb ",
            url: "https://static.wikia.nocookie.net/esstarwars/images/1/14/Old_nien_nunb_-_profile.png ",
        },
        {
            id: 32,
            name: "Qui-Gon Jinn",
            url: "https://static.wikia.nocookie.net/esstarwars/images/8/86/QuiGon.jpg",

        },
        {
            id: 33,
            name: "Nute Gunray",
            url: "https://static.wikia.nocookie.net/esstarwars/images/b/b6/Nutegunraygeonosis.jpg",

        },
        {
            id: 34,
            name: "Finis Valorum",
            url: "https://static.wikia.nocookie.net/esstarwars/images/8/89/Valorum.jpg",

        },
        {
            id: 35,
            name: "Padmé Amidala",
            url: "https://static.wikia.nocookie.net/doblaje/images/6/64/Padmestarwarsay4y46.png",

        },
        {
            id: 36,
            name: "Jar Jar Binks",
            url: "https://i.blogs.es/05d040/espinof-star-wars-jarjarbinks/1366_2000.jpg",

        },
        {
            id: 37,
            name: "Roos Tarpals",
            url: "https://static.wikia.nocookie.net/esstarwars/images/c/ca/TarpalsHS.jpg",

        },
        {
            id: 38,
            name: "Rugor Nass",
            url: "https://static.wikia.nocookie.net/esstarwars/images/d/d8/Bossnass.jpg",

        },
        {
            id: 39,
            name: "Ric Olié",
            url: "https://shttps://static.wikia.nocookie.net/esstarwars/images/8/8a/RicOlie.jpg",

        },
        {
            id: 40,
            name: "Watto",
            url: "https://static.wikia.nocookie.net/esstarwars/images/e/e2/WattoEp1TPM.jpg",

        },
        {
            id: 41,
            name: "Sebulba",
            url: "https://static.wikia.nocookie.net/esstarwars/images/1/14/Sebulba_Headshot_Closeup.png",

        },
        {
            id: 42,
            name: "Quarsh Panaka",
            url: "https://static.wikia.nocookie.net/esstarwars/images/7/72/PanakaHS-TPM.png",

        },
        {
            id: 43,
            name: "Shmi Skywalker",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/98/ShmiSkywalkerDatabank_%28Repurposed%29.jpg",

        },
        {
            id: 44,
            name: "Darth Maul",
            url: "https://static.wikia.nocookie.net/esstarwars/images/5/50/Darth_Maul_profile.png",

        },
        {
            id: 45,
            name: "Bib Fortuna",
            url: "https://static.wikia.nocookie.net/esstarwars/images/3/33/BibFortunaHS-ROTJ.png",

        },
        {
            id: 46,
            name: "Ayla Secura",
            url: "https://static.wikia.nocookie.net/esstarwars/images/f/f9/Aayla.jpg",

        },
        {
            id: 47,
            name: "Ratts Tyerel",
            url: "https://static.wikia.nocookie.net/starwars/images/6/68/RattsHS.jpg",

        },
        {
            id: 48,
            name: "Dud Bolt",
            url: "https://static.wikia.nocookie.net/starwars/images/b/b0/Dud_Bolt.jpg",

        },
        {
            id: 49,
            name: "Gasgano",
            url: "https://static.wikia.nocookie.net/esstarwars/images/3/30/Gasgano.jpg",

        },
        {
            id: 50,
            name: "Ben Quadinaros",
            url: "https://static.wikia.nocookie.net/esstarwars/images/7/7f/Cropped_Quadinaros.png",

        },
        {
            id: 51,
            name: "Mace Windu",
            url: "https://wipy.tv/wp-content/uploads/2020/07/mace-windu-1200x900.jpg",

        },
        {
            id: 52,
            name: "Ki-Adi-Mundi",
            url: "https://pm1.narvii.com/6282/88af2eb9af8088f2e722e8f884748d57423f9de6_hq.jpg",

        },
        {
            id: 53,
            name: "Kit Fisto",
            url: "http://pm1.narvii.com/6682/21920397f27d3a939a41938f3f71a807c72d7df3_00.jpg",

        },
        {
            id: 54,
            name: "Eeth Koth",
            url: "https://static.wikia.nocookie.net/esstarwars/images/4/4e/EethKothCardTrader.png",

        },
        {
            id: 55,
            name: "Adi Gallia",
            url: "https://static.wikia.nocookie.net/doblaje/images/4/45/Adi_Gallia_-_personaje.png",

        },
        {
            id: 56,
            name: "Saesee Tiin",
            url: "https://i.pinimg.com/originals/fa/64/1b/fa641b222be88e546446e158da3a3a4f.png",

        },
        {
            id: 57,
            name: "Yarael Poof",
            url: "https://static.wikia.nocookie.net/esstarwars/images/4/4f/Yarael_Poof_USW.png",

        },
        {
            id: 58,
            name: "Plo Koon",
            url: "https://static.wikia.nocookie.net/esstarwars/images/b/bf/PloKoonCardTrader.png",

        },
        {
            id: 59,
            name: "Mas Amedda",
            url: "https://static.wikia.nocookie.net/esstarwars/images/3/37/Mas_Amedda_SWCT.png",

        },
        {
            id: 60,
            name: "Gregar Typho",
            url: "https://static.wikia.nocookie.net/headhuntersholosuite/images/5/52/Gregar_Typho.jpg",

        },
        {
            id: 61,
            name: "Cordé",
            url: "https://static.wikia.nocookie.net/banthapedia/images/0/08/Corde.jpg",

        },
        {
            id: 62,
            name: "Cliegg Lars",
            url: "http://4.bp.blogspot.com/-2L9bw83A0pI/TtS5eoSDPnI/AAAAAAAANIA/z03x5wE6wtE/s1600/Cliegg.jpg",

        },
        {
            id: 63,
            name: "Poggle the Lesser",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/93/Poggle_the_lesser_-_sw_card_trader.png",

        },
        {
            id: 64,
            name: "Luminara Unduli",
            url: "https://static.wikia.nocookie.net/doblaje/images/7/7d/LUMINARA_UNDULI_STAR_WARS_MOVIES.png",

        },
        {
            id: 65,
            name: "Barriss Offee",
            url: "https://static.wikia.nocookie.net/esstarwars/images/3/37/Barrisprofile2.jpg",

        },
        {
            id: 66,
            name: "Dormé",
            url: "https://static.wikia.nocookie.net/esstarwars/images/8/8b/Dorme.jpg",

        },
        {
            id: 67,
            name: "Dooku",
            url: "https://static.wikia.nocookie.net/doblaje/images/8/84/CondeDooku_star_wars.png",

        },
        {
            id: 68,
            name: "Bail Prestor Organa",
            url: "https://static.wikia.nocookie.net/esstarwars/images/5/50/Bail_Organa_Mug.jpg",

        },
        {
            id: 69,
            name: "Jango Fett",
            url: "https://static.wikia.nocookie.net/esstarwars/images/6/6e/286334-10659-jango-fett_large-0.jpg",

        },
        {
            id: 70,
            name: "Zam Wesell",
            url: "https://static.wikia.nocookie.net/esstarwars/images/c/c0/Zamaotc.jpg",

        },
        {
            id: 71,
            name: "Dexter Jettster",
            url: "https://static.wikia.nocookie.net/esstarwars/images/8/83/Dexter_Jettster_%28Besalisk%29_FF44.jpg",

        },
        {
            id: 72,
            name: "Lama Su",
            url: "https://static.wikia.nocookie.net/esstarwars/images/7/73/Lama_Su.jpg",

        },
        {
            id: 73,
            name: "Taun We",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/9c/TaunWe.jpg",

        },
        {
            id: 74,
            name: "Jocasta Nu",
            url: "https://static.wikia.nocookie.net/esstarwars/images/1/15/Jocasta_Nu_SWE.png",

        },
        {
            id: 75,
            name: "R4-P17",
            url: "https://static.wikia.nocookie.net/starwars/images/5/52/R4-P17_USWNE.png",

        },
        {
            id: 76,
            name: "Wat Tambor",
            url: "https://static.wikia.nocookie.net/esstarwars/images/6/6f/Wattambor_detail.png",

        },
        {
            id: 77,
            name: "San Hill",
            url: "https://static.wikia.nocookie.net/esstarwars/images/4/4f/San_hill.jpg",

        },
        {
            id: 78,
            name: "Shaak Ti",
            url: "https://static.wikia.nocookie.net/esstarwars/images/2/20/Shaak_Ti_closeup-SWE.png",

        },
        {
            id: 79,
            name: "Grievous",
            url: "https://sm.ign.com/t/ign_latam/screenshot/default/clipboard02_c844.1280.jpg",

        },
        {
            id: 80,
            name: "Tarfful",
            url: "https://static.wikia.nocookie.net/esstarwars/images/3/37/Tarfful_RotS.png",

        },
        {
            id: 81,
            name: "Raymus Antilles",
            url: "https://static.wikia.nocookie.net/esstarwars/images/6/61/RaymusAntilles.jpg",

        },
        {
            id: 82,
            name: "Sly Moore",
            url: "https://static.wikia.nocookie.net/esstarwars/images/9/9f/Sly_moore.jpg",

        },
        {
            id: 83,
            name: "Tion Medon",
            url: "https://static.wikia.nocookie.net/esstarwars/images/e/e8/TionMedonchron.jpg",

        },

    ];

    imgCharacters.forEach(element => {

        if (starWarsNameId == element.id) {
            urlImg = element.url;
            // console.log(urlImg);
            return urlImg;
        }

        return urlImg;
    });
    // console.log(urlImg);
    return urlImg;
}


export { findImgCharacters };