// import { displayListCharacter } from "../view/generalList"
import { fetchStarWarsChrtPage } from "./api-listall";
import { fetchStarWarsPlntsPage } from "./api-listall";
import { fetchStarWarsStshpPage } from "./api-listall"




// -------------------------------------RELOAD PAGE-------------------------------------------
let reloadPage = () => {
    location.reload();
};



// ----------------------------------------LOADER-------------------------------
const starWarsLoader = () => {
    document.querySelector('.starWarsLoader').classList.toggle('hide');

};

// ------------------------------------DYNAMIC TEXT WRITTER-------------------------------
var i = 0;
//Text
var txt = 'welcome to wikiwars...in a website far far away...'
    // The speed/duration of the effect in milliseconds 
var speed = 100;

function typeWriter() {
    if (i < txt.length) {
        document.getElementById('pageTittle').innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}

//------------------------------------------------------------------CHARACTER BUTTONS----------------------------------------------
let pageChrt = 1; //Esta variable varia en funcion de las veces que entremos en los botones"solo vale 1 la primera vez"

const handlePrewChrt = () => {

    pageChrt--;
    const url = `https://swapi.dev/api/people/?page=${pageChrt}`;

    const divUls = document.getElementById("divUls");
    const ul1 = document.getElementById("starWarsList1");

    fetchStarWarsChrtPage(url)
        .then((character) => {
            ul1.innerHTML = "";
            // displayListCharacter(character);
            character.forEach(character => {
                ul1.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${character.name}</span><span>Id: ${character.id}</span></li>`;
                divUls.appendChild(ul1);
            });
            //Dinamics Buttons CHRT
            const divButton = document.createElement("div");
            divButton.innerHTML = "";
            divButton.classList.add("container-fluid");

            const btnPrewChrts = document.createElement("button");
            btnPrewChrts.classList.add("generic-button");
            btnPrewChrts.textContent = "Prew Page";
            //EVENT
            btnPrewChrts.addEventListener("click", handlePrewChrt);
            divButton.appendChild(btnPrewChrts);

            const btnNextChrts = document.createElement("button");
            btnNextChrts.classList.add("generic-button");
            btnNextChrts.textContent = "Next Page";
            //EVENT
            btnNextChrts.addEventListener("click", handleNextChrt);
            divButton.appendChild(btnNextChrts);


            ul1.appendChild(divButton);
        });
};

const handleNextChrt = () => {

    pageChrt++;
    const url = `https://swapi.dev/api/people/?page=${pageChrt}`;

    const divUls = document.getElementById("divUls");
    const ul1 = document.getElementById("starWarsList1");
    fetchStarWarsChrtPage(url)
        .then((character) => {
            ul1.innerHTML = "";
            // displayListCharacter(character);
            character.forEach(character => {
                ul1.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${character.name}</span><span>Id: ${character.id}</span></li>`;
                divUls.appendChild(ul1);
            });
            //Dinamics Buttons CHRT
            const divButton = document.createElement("div");
            divButton.innerHTML = "";
            divButton.classList.add("container-fluid");

            const btnPrewChrts = document.createElement("button");
            btnPrewChrts.classList.add("generic-button");
            btnPrewChrts.textContent = "Prew Page";
            //EVENT
            btnPrewChrts.addEventListener("click", handlePrewChrt);
            divButton.appendChild(btnPrewChrts);

            const btnNextChrts = document.createElement("button");
            btnNextChrts.classList.add("generic-button");
            btnNextChrts.textContent = "Next Page";
            //EVENT
            btnNextChrts.addEventListener("click", handleNextChrt);
            divButton.appendChild(btnNextChrts);


            ul1.appendChild(divButton);
        });

}

//------------------------------------------------------------------PLANETS BUTTONS----------------------------------------------
let pagePlnts = 1;

const handlePrewPlnts = () => {
    pagePlnts--;
    const url = `https://swapi.dev/api/planets/?page=${pagePlnts}`;

    const divUls = document.getElementById("divUls");
    const ul2 = document.getElementById("starWarsList2");

    fetchStarWarsPlntsPage(url)
        .then((planets) => {
            ul2.innerHTML = "";
            // displayListCharacter(character);
            planets.forEach(planets => {
                ul2.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${planets.name}</span><span>Id: ${planets.id}</span></li>`;
                divUls.appendChild(ul2);
            });
            //Dinamics Buttons CHRT
            const divButton = document.createElement("div");
            divButton.innerHTML = "";
            divButton.classList.add("container-fluid");

            const btnPrewPlnts = document.createElement("button");
            btnPrewPlnts.classList.add("generic-button");
            btnPrewPlnts.textContent = "Prew Page";
            //EVENT
            btnPrewPlnts.addEventListener("click", handlePrewPlnts);
            divButton.appendChild(btnPrewPlnts);

            const btnNextPlnts = document.createElement("button");
            btnNextPlnts.classList.add("generic-button");
            btnNextPlnts.textContent = "Next Page";
            //EVENT
            btnNextPlnts.addEventListener("click", handleNextPlnts);
            divButton.appendChild(btnNextPlnts);


            ul2.appendChild(divButton);
        });
};

const handleNextPlnts = () => {
    pagePlnts++;
    const url = `https://swapi.dev/api/planets/?page=${pagePlnts}`;

    const divUls = document.getElementById("divUls");
    const ul2 = document.getElementById("starWarsList2");

    fetchStarWarsPlntsPage(url)
        .then((planets) => {
            ul2.innerHTML = "";
            // displayListCharacter(character);
            planets.forEach(planets => {
                ul2.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${planets.name}</span><span>Id: ${planets.id}</span></li>`;
                divUls.appendChild(ul2);
            });
            //Dinamics Buttons CHRT
            const divButton = document.createElement("div");
            divButton.innerHTML = "";
            divButton.classList.add("container-fluid");

            const btnPrewPlnts = document.createElement("button");
            btnPrewPlnts.classList.add("generic-button");
            btnPrewPlnts.textContent = "Prew Page";
            //EVENT
            btnPrewPlnts.addEventListener("click", handlePrewPlnts);
            divButton.appendChild(btnPrewPlnts);

            const btnNextPlnts = document.createElement("button");
            btnNextPlnts.classList.add("generic-button");
            btnNextPlnts.textContent = "Next Page";
            //EVENT
            btnNextPlnts.addEventListener("click", handleNextPlnts);
            divButton.appendChild(btnNextPlnts);


            ul2.appendChild(divButton);
        });

}

//------------------------------------------------------------------STARSHIP BUTTONS----------------------------------------------
let pageStshp = 1;

const handlePrewStshp = () => {
    pageStshp--;
    const url = `https://swapi.dev/api/starships/?page=${pageStshp}`;

    const divUls = document.getElementById("divUls");
    const ul3 = document.getElementById("starWarsList3");

    fetchStarWarsStshpPage(url)
        .then((starships) => {
            ul3.innerHTML = "";
            // displayListCharacter(character);
            starships.forEach(starships => {
                ul3.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${starships.name}</span><span>Id: ${starships.id}</span></li>`;
                divUls.appendChild(ul3);
            });
            //Dinamics Buttons CHRT
            const divButton = document.createElement("div");
            divButton.innerHTML = "";
            divButton.classList.add("container-fluid");

            const btnPrewStshp = document.createElement("button");
            btnPrewStshp.classList.add("generic-button");
            btnPrewStshp.textContent = "Prew Page";
            //EVENT
            btnPrewStshp.addEventListener("click", handlePrewStshp);
            divButton.appendChild(btnPrewStshp);

            const btnNextStshp = document.createElement("button");
            btnNextStshp.classList.add("generic-button");
            btnNextStshp.textContent = "Next Page";
            //EVENT
            btnNextStshp.addEventListener("click", handleNextStshp);
            divButton.appendChild(btnNextStshp);


            ul3.appendChild(divButton);
        });
};


const handleNextStshp = () => {
    pageStshp++;
    const url = `https://swapi.dev/api/starships/?page=${pageStshp}`;

    const divUls = document.getElementById("divUls");
    const ul3 = document.getElementById("starWarsList3");

    fetchStarWarsStshpPage(url)
        .then((starships) => {
            ul3.innerHTML = "";
            // displayListCharacter(character);
            starships.forEach(starships => {
                ul3.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${starships.name}</span><span>Id: ${starships.id}</span></li>`;
                divUls.appendChild(ul3);
            });
            //Dinamics Buttons CHRT
            const divButton = document.createElement("div");
            divButton.innerHTML = "";
            divButton.classList.add("container-fluid");

            const btnPrewStshp = document.createElement("button");
            btnPrewStshp.classList.add("generic-button");
            btnPrewStshp.textContent = "Prew Page";
            //EVENT
            btnPrewStshp.addEventListener("click", handlePrewStshp);
            divButton.appendChild(btnPrewStshp);

            const btnNextStshp = document.createElement("button");
            btnNextStshp.classList.add("generic-button");
            btnNextStshp.textContent = "Next Page";
            //EVENT
            btnNextStshp.addEventListener("click", handleNextStshp);
            divButton.appendChild(btnNextStshp);


            ul3.appendChild(divButton);
        });

}

export { reloadPage };
export { starWarsLoader };
export { handlePrewChrt };
export { handleNextChrt };
export { handleNextPlnts };
export { handlePrewPlnts }
export { handleNextStshp };
export { handlePrewStshp };
export { typeWriter };