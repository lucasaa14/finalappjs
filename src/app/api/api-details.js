import { starWarsLoader } from "./functions";




const fetchStarWarsCharactersDetails = async(id) => {
    starWarsLoader();
    const url = `https://swapi.dev/api/people/${id}`;
    const promise = await fetch(url);
    const response = await promise.json();
    const details = response;
    starWarsLoader();
    // console.log(details);
    return details;

};
export { fetchStarWarsCharactersDetails };