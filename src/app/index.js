import './styles/styles.scss';
import 'bootstrap';
// import { displayList } from './view/generalList';
// import { hideCharacter } from './api/functions';
// import { hideCharacter } from './api/functions';

import { displayListCharacter } from "./view/generalList";
import { displayListPlanets } from "./view/generalList";
import { displayListStarships } from "./view/generalList";
import { displayListDetails } from "./view/listDetails";
import { displayListFilms } from './view/generalList';
import { reloadPage } from "./api/functions";
import { typeWriter } from "./api/functions"



function addListeners() {
    // document.getElementById("btnChrt").addEventListener("click", displayList);
    document.getElementById("btnChrt").addEventListener("click", displayListCharacter);
    document.getElementById("btnPlnt").addEventListener("click", displayListPlanets);
    document.getElementById("btnStshp").addEventListener("click", displayListStarships);
    document.getElementById("finderStarWars").addEventListener("click", reloadPage);
    document.getElementById("btnSearch").addEventListener("click", displayListDetails);
    document.getElementById("btnFilms").addEventListener("click", displayListFilms);

    // console.log('addListeners');
}

window.onload = function() {
    addListeners();
    typeWriter();


};