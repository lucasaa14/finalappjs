import { isXMLDoc } from "jquery";
import { fetchStarWarsCharacters } from "../api/api-listall";
import { fetchStarWarsPlanets } from "../api/api-listall";
import { fetchStarWarsStarships } from "../api/api-listall";
import { handleNextChrt } from "../api/functions";
import { handlePrewChrt } from "../api/functions";
import { handleNextPlnts } from "../api/functions";
import { handlePrewPlnts } from "../api/functions";
import { handleNextStshp } from "../api/functions";
import { handlePrewStshp } from "../api/functions";
import { starWarsFilm } from "../api/api-Films";



//----------------------------------PRINT CHARACTERS--------------------------------
const displayListCharacter = () => {
    const divUls = document.getElementById("divUls");
    const detailList = document.getElementById("detailList");
    const ul4 = document.getElementById("starWarsList4");
    detailList.innerHTML = "";
    ul4.innerHTML = "";
    const ul1 = document.getElementById("starWarsList1");
    ul1.classList.add("starWarsList1");
    if (ul1.children.length > 0) {
        ul1.classList.toggle("hide");
    } else {
        fetchStarWarsCharacters()
            .then((list) => {
                //list es igual al "return" del fetch, es decir "characterInfo"
                //es un objeto {name,id}
                list.forEach(character => {
                    ul1.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${character.name}</span><span>  Id:${character.id}</span></li>`;
                    divUls.appendChild(ul1);
                });
                //Dinamics Buttons CHRT
                const divButton = document.createElement("div");
                divButton.innerHTML = "";
                divButton.classList.add("container-fluid");

                const btnPrewChrts = document.createElement("button");
                btnPrewChrts.classList.add("generic-button");
                btnPrewChrts.textContent = "Prew Page";
                //EVENT
                btnPrewChrts.addEventListener("click", handlePrewChrt);
                divButton.appendChild(btnPrewChrts);

                const btnNextChrts = document.createElement("button");
                btnNextChrts.classList.add("generic-button");
                btnNextChrts.textContent = "Next Page";
                //EVENT
                btnNextChrts.addEventListener("click", handleNextChrt);
                divButton.appendChild(btnNextChrts);


                ul1.appendChild(divButton);
            });
    }
}



//----------------------------------PRINT PLANETS--------------------------------
const displayListPlanets = () => {
    const divUls = document.getElementById("divUls");
    const detailList = document.getElementById("detailList");
    const ul4 = document.getElementById("starWarsList4");
    ul4.innerHTML = "";
    detailList.innerHTML = "";
    const ul2 = document.getElementById("starWarsList2");
    ul2.classList.add("starWarsList2");
    // ul.innerHTML = ""; Lo usamos cuando el mismo boton nos borre y muestro las otras page
    if (ul2.children.length > 0) {
        ul2.classList.toggle("hide");
    } else {
        fetchStarWarsPlanets()
            .then((list) => {
                //list es igual al "return" del fetch, es decir "planetsInfo"
                //es un objeto {name,id}
                list.forEach(planets => {
                    ul2.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${planets.name}</span><span>  Id:${planets.id}</span></li>`;
                    divUls.appendChild(ul2);
                });
                //Dinamics Buttons PLNT
                const divButton = document.createElement("div");
                divButton.innerHTML = "";
                divButton.classList.add("container-fluid");

                const btnPrewPlnt = document.createElement("button");
                btnPrewPlnt.classList.add("generic-button");
                btnPrewPlnt.textContent = "Prew Page";
                //EVENT
                btnPrewPlnt.addEventListener("click", handlePrewPlnts);
                divButton.appendChild(btnPrewPlnt);

                const btnNextPlnt = document.createElement("button");
                btnNextPlnt.classList.add("generic-button");
                btnNextPlnt.textContent = "Next Page";
                //EVENT
                btnNextPlnt.addEventListener("click", handleNextPlnts);
                divButton.appendChild(btnNextPlnt);




                ul2.appendChild(divButton);



            });

    }

};


//----------------------------------PRINT  STARSHIPS--------------------------------
const displayListStarships = () => {
    const divUls = document.getElementById("divUls");
    const detailList = document.getElementById("detailList");
    const ul4 = document.getElementById("starWarsList4");
    ul4.innerHTML = "";
    detailList.innerHTML = "";
    const ul3 = document.getElementById("starWarsList3");
    ul3.classList.add("starWarsList3");
    // ul.innerHTML = ""; Lo usamos cuando el mismo boton nos borre y muestro las otras page
    if (ul3.children.length > 0) {
        ul3.classList.toggle("hide");
    } else {
        fetchStarWarsStarships()
            .then((list) => {
                //list es igual al "return" del fetch, es decir "starShipsInfo"
                //es un objeto {name,id}
                list.forEach(starships => {
                    ul3.innerHTML += `<li class="listStarWars__div"> <span>Nombre:${starships.name}</span><span>  Id:${starships.id}</span></li>`;
                    divUls.appendChild(ul3);
                });
                //Dinamics Buttons STSHP
                const divButton = document.createElement("div");
                divButton.innerHTML = "";
                divButton.classList.add("container-fluid");

                const btnPrewStshp = document.createElement("button");
                btnPrewStshp.classList.add("generic-button");
                btnPrewStshp.textContent = "Prew Page";
                //EVENT
                btnPrewStshp.addEventListener("click", handlePrewStshp);
                divButton.appendChild(btnPrewStshp);

                const btnNextStshp = document.createElement("button");
                btnNextStshp.classList.add("generic-button");
                btnNextStshp.textContent = "Next Page";
                //EVENT
                btnNextStshp.addEventListener("click", handleNextStshp);
                divButton.appendChild(btnNextStshp);




                ul3.appendChild(divButton);

            });

    }


};

//----------------------------------PRINT  FILMS--------------------------------

const displayListFilms = () => {
    const divUls = document.getElementById("divUls");
    const ul1 = document.getElementById("starWarsList1");
    const ul2 = document.getElementById("starWarsList2");
    const ul3 = document.getElementById("starWarsList3");
    const ul4 = document.getElementById("starWarsList4");
    const detailList = document.getElementById("detailList");
    ul1.innerHTML = "";
    ul2.innerHTML = "";
    ul3.innerHTML = "";
    ul4.innerHTML = "";
    detailList.innerHTML = "";
    ul4.classList.add("starWarsList1");
    if (ul4.children.length > 0) {
        ul4.classList.toggle("hide");
    } else {
        starWarsFilm.forEach(films => {
            ul4.innerHTML += `<li class="listStarWars__div-films"> 
            <span>Titulo:${films.tittle}</span>
            <span>  Year:${films.year}</span>
            <span>  Director:${films.director}</span>
            <span>  Reparto:${films.casting}</span>
            <span>  Sinopsis:${films.sinopsis}</span>
            <div class="divFilms_img"><img src=${films.img} class="detailList_img"></div>
            </li>`;
            divUls.appendChild(ul4);
        });



    };
}

export { displayListCharacter };
export { displayListPlanets };
export { displayListStarships };
export { displayListFilms };