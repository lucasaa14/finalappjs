import { fetchStarWarsCharactersDetails } from "../api/api-details"
import { findImgCharacters } from "../api/api-IMGCharacters"


let displayListDetails = () => {

    const divUls = document.getElementById("divUls");
    const ul1 = document.getElementById("starWarsList1");
    const ul2 = document.getElementById("starWarsList2");
    const ul3 = document.getElementById("starWarsList3");
    const ul4 = document.getElementById("starWarsList4");
    const detailList = document.getElementById("detailList");
    ul1.innerHTML = "";
    ul2.innerHTML = "";
    ul3.innerHTML = "";
    ul4.innerHTML = "";

    const starWarsNameId = document.getElementById("bowser").value;
    fetchStarWarsCharactersDetails(starWarsNameId)
        .then((details) => {
            // console.log(details);
            detailList.innerHTML = "";
            const pDetails = document.createElement("p");
            pDetails.innerHTML = "";
            pDetails.classList.add("listStarWars__divdetails");
            let urlmg = findImgCharacters(starWarsNameId);
            // console.log(urlmg);
            // console.log(details);

            pDetails.innerHTML = `<li>Name:${details.name}</li>
                     <li>Height:${details.height}</li>
                     <li>Mass:${details.mass}</li>
                    <li>Birth Year:${details.birth_year}</li>
                     <li>Gender:${details.gender}</li>
                    <li>Skin Color:${details.skin_color}</li>
                   <img src=${urlmg} class="detailList_img">`;
            detailList.appendChild(pDetails);
        });
};
export { displayListDetails };


// Otra forma de resolverlo pero por "Nombre del personaje"
//         pDetails.innerHTML = "";
// details.forEach(characters => {
//     characters.name.toUpperCase()
//         // console.log(characters);
//     if (characters.name.includes(starWarsName.value)) {
//         detailList.innerHTML = "";
//         console.log(characters);
//         const pDetails = document.createElement("p");
//         pDetails.innerHTML = "";
//         pDetails.classList.add("listStarWars__divdetails");
//         console.log(characters.name);
//         let urlmg = findImgCharacters(characters.name);

//         //Result=return de findImgCharacters
//         console.log(urlmg);
//         pDetails.innerHTML = `<li>Name:${characters.name}</li>
//         <li>Height:${characters.height}</li>
//         <li>Mass:${characters.mass}</li>
//         <li>Birth Year:${characters.birth_year}</li>
//         <li>Gender:${characters.gender}</li>
//         <li>Skin Color:${characters.skin_color}</li>
//         <img src=${urlmg}>`;
//         detailList.appendChild(pDetails);
//     } else {
//         const pDetails = document.createElement("p");
//         pDetails.innerHTML = "";
//         pDetails.classList.add("listStarWars__div");
//         pDetails.textContent = "Su personaje no existe";
//     }
// });